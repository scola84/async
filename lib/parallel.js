'use strict';

class Parallel {
  constructor() {
    this.processor = null;
  }

  setProcessor(processor) {
    this.processor = processor;
    return this;
  }

  process(items, parameters) {
    return Promise.all(items.map((item) => {
      return this.processor(item, parameters);
    }));
  }
}

module.exports = Parallel;
