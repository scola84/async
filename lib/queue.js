'use strict';

const EventHandler = require('@scola/events');

class Queue extends EventHandler {
  constructor() {
    super();

    this.queue = new Set();
    this.processor = null;
    this.iterator = null;
  }

  setProcessor(processor) {
    this.processor = processor;
    return this;
  }

  add(item) {
    this.queue.add(item);
    this.run();

    return this;
  }

  delete(item) {
    this.queue.delete(item);
    return this;
  }

  run() {
    if (this.iterator) {
      return;
    }

    this.iterator = new Set(this.queue).values();

    this.emit('run');
    this.process();
  }

  process() {
    const item = this.iterator.next();

    if (item.done) {
      this.finish();
      return;
    }

    this.processor(item.value)
      .then(this.handleProcess.bind(this, item))
      .catch(this.handleError.bind(this));
  }

  finish() {
    this.iterator = null;

    if (this.queue.size === 0) {
      this.emit('drain');
      return;
    }

    this.run();
  }

  handleProcess(item) {
    this.queue.delete(item.value);
    this.process();
  }

  handleError(error) {
    this.iterator = null;
    this.emit('error', error);
  }
}

module.exports = Queue;
