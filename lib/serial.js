'use strict';

class Serial {
  constructor() {
    this.processor = null;
  }

  setProcessor(processor) {
    this.processor = processor;
    return this;
  }

  process(items, parameters) {
    const results = [];
    let rejected = false;

    return new Promise((resolve, reject) => {
      items.reduce((promise, next) => {
        return promise.then(() => {
          return !rejected &&
            this.processor(next, parameters, results);
        }).then((result) => {
          results.push(result);
          return results;
        }).catch((error) => {
          reject(error);
          rejected = true;
        });
      }, Promise.resolve()).then(() => {
        return !rejected &&
          resolve(results);
      });
    });
  }
}

module.exports = Serial;
