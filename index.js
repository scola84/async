module.exports = {
  Parallel: require('./lib/parallel'),
  Queue: require('./lib/queue'),
  Serial: require('./lib/serial')
};
